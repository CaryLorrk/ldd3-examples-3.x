
SUBDIRS =  misc-progs misc-modules \
           skull scull scullc sculld scullp scullv sbull snull\
	   short shortprint pci simple usb tty lddbus

CLEAN_SUBDIRS = $(SUBDIRS:%=clean-%)

CONFIG_SHELL := $(shell if [ -x "$$BASH" ]; then echo $$BASH; \
		else if [ -x /bin/bash ]; then echo /bin/bash; \
		else echo sh; fi ; fi)

export SRCTREE := $(shell pwd)

all: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@


###
# Cleaning is done on three levels.
# make clean Delete most generated files
# Leave enough to build external modules
# make mrproper Delete the current configuration, and all generated files
# make distclean Remove editor backup files, patch leftover files and the like

clean: $(CLEAN_SUBDIRS)

$(CLEAN_SUBDIRS):
	$(MAKE) -C $(@:clean-%=%) clean

mrproper: clean
	rm -rf tags TAGS cscope* GPATH GTAGS GRTAGS GSYMS

distclean: mrproper
	@find $(SRCTREE) \
			\( -name '*.orig' -o -name '*.rej' -o -name '*~' \
			-o -name '*.bak' -o -name '#*#' -o -name '.*.orig' \
			-o -name '.*.rej' -o -name '*%' -o -name 'core' \) \
			-type f -print | xargs rm -f

cmd_tags = $(CONFIG_SHELL) scripts/tags.sh $@ 
tags TAGS cscope gtags:
	$(call cmd_tags)

.PHONY: all $(SUBDIRS) \
	clean $(CLEAN_SUBDIRS) mrproper distclean \
	tags TAGS cscope gtags
