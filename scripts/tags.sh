#!/bin/sh

KERNELDIR=/lib/modules/`uname -r`/build

ldd3_sources() 
{
    find $SRCTREE/* -name *.mod.c -prune -o -name *.c -print -o -name *.h -print
}

all_sources()
{
    ldd3_sources
}

docscope()
{
    (echo \-k; echo \-q; all_sources) > cscope.files
    cscope -b -f cscope.out
}
dogtags()
{
    all_sources | gtags -i -f -
}


xtags()
{
    all_sources | xargs $1 -a
}

case "$1" in
    "cscope")
        docscope
        ;;
    "gtags")
        dogtags
        ;;
    "tags")
        rm -f tags
        xtags ctags
        ;;
    "TAGS")
        rm -rf TAGS
        xtags etags
        ;;
esac
sudo make -C $KERNELDIR $1
